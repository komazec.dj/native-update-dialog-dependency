# Native Update Dialog Dependency

Simple dependency made for Flutter that allows developers to embed Android native update dialogs for the app.  
Native update dialogs are not yet supported by iOS.